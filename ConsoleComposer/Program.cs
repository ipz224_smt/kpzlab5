﻿
using ComposerClassLibrary;

class Program
{
    static void Main(string[] args)
    {

        var paragraph = new LightElementNode("p", "block", "normal", new List<string> { "paragraph" }, new List<LightNode>
        {
            new LightTextNode("This is a paragraph.")
        });

        var div = new LightElementNode("div", "block", "normal", new List<string> { "container" }, new List<LightNode>
        {
            paragraph,
            new LightElementNode("span", "inline", "normal", new List<string>(), new List<LightNode>
            {
                new LightTextNode("This is a span inside a div.")
            })
        });

        Console.WriteLine("OuterHTML:");
        Console.WriteLine(div.OuterHTML);
        Console.WriteLine("\nInnerHTML:");
        Console.WriteLine(div.InnerHTML);
        Console.WriteLine("\n");

        var table = new LightElementNode("table", "block", "normal", new List<string> { "table" }, new List<LightNode>
    {

        new LightElementNode("tr", "block", "normal", new List<string>(), new List<LightNode>
        {

            new LightElementNode("td", "block", "normal", new List<string>(), new List<LightNode>
            {
                new LightTextNode("Рядок 1, Комірка 1")
            }),

            new LightElementNode("td", "block", "normal", new List<string>(), new List<LightNode>
            {
                new LightTextNode("Рядок 1, Комірка 2")
            })
        }),

        new LightElementNode("tr", "block", "normal", new List<string>(), new List<LightNode>
        {

            new LightElementNode("td", "block", "normal", new List<string>(), new List<LightNode>
            {
                new LightTextNode("Рядок 2, Комірка 1")
            }),

            new LightElementNode("td", "block", "normal", new List<string>(), new List<LightNode>
            {
                new LightTextNode("Рядок 2, Комірка 2")
            })
        })
    });


        Console.WriteLine("Згенерований HTML елемент:");
        Console.WriteLine(table.OuterHTML);
    }
}